<!DOCTYPE html>
<html>
<head>
	<title>MyArtits Search</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="style.css">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<h1 style="font-size: 500%; text-align: center">&#120080;yartist</h1>
<?php
require_once 'connect.php'
?>
</head>
<body>
	<h1>Search Results</h1>
	<div>
<?php
	if (isset($_GET['submit-search'])) { 
		$search = mysqli_real_escape_string($mysqli, $_GET['search']);
		$result = mysqli_query($mysqli, "SELECT id, name, style, platform, description FROM artist WHERE name LIKE '%$search%' OR style LIKE '%$search%'  OR platform LIKE '%$search%' OR description LIKE '%$search%'");
		$queryResults = mysqli_num_rows($result);

		echo "<h3>".$queryResults." Result(s)"."</h3>";
?>
<?php } ?>
<?php 		if ($queryResults > 0) { ?>
	<div class="container">
	 <table class="table" style="background-color: rgba(20, 20, 20, 0.4); width: 75%">
    	<thead>
    		<tr>
    			<th>Name</th>
    			<th>Style</th>
    			<th>Platform</th>
    			<th>Description</th>
    			<th colspan="3" style="text-align: center;">More</th>
    		</tr>
    	</thead>
			<div>
            <?php 
            while ($row = mysqli_fetch_array($result)) { ?>
		<tr onmouseover="style='background-color: white; color:black'" onmouseleave="style='background-color: rgba(20, 20, 20, 0.2); color: white'">			
			<td><?php $string = $row['name'];
		if (strlen($string) > 20) {
			$trimstring = substr($string, 0, 20). '...';
			} else {
			$trimstring = $string;
			}
			echo $trimstring; ?></td>
			<td><?php $string = $row['style'];
		if (strlen($string) > 12) {
			$trimstring = substr($string, 0, 12). '...';
			} else {
			$trimstring = $string;
			}
			echo $trimstring; ?></td>
			<td><?php $string = $row['platform'];
		if (strlen($string) > 12) {
			$trimstring = substr($string, 0, 12). '...';
			} else {
			$trimstring = $string;
			}
			echo $trimstring; ?></td>
			<td><?php 
			 $string = $row['description'];
		if (strlen($string) > 12) {
			$trimstring = substr($string, 0, 12). '...';
			} else {
			$trimstring = $string;
			}
			echo $trimstring;
			?>
			</td>
			<td>
				<a href="detail.php?detail=<?php echo $row['id']; ?>" class="detail_btn" >&#128466;</a>
			</td>
			<td>
				<a href="edit.php?edit=<?php echo $row['id']; ?>" class="edit_btn" >&#128396;</a>
			</td>
			<td>
				<a href="index.php?del=<?php echo $row['id']; ?>" class="del_btn" onclick="return confirm('Are you sure you want to Delete this?');">&#128465;</a>
			</td>
		</tr>
	<?php } ?>
           </table>
	<?php } ?>		

	</div>
	<a href="#" onclick="history.back();"><button type="button" name="back" class="btn">Back</button></a>
</body>
</html>


