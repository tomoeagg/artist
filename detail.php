<html>
	<head>
		<title>MyArtist Details</title>
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="style.css">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<h1 style="font-size: 500%;">&#120080;yartist</h1>
<?php require_once 'connect.php'
?>
	</head>
<body>
	<h1>Artist's details</h1>
<?php

	$id = $_GET['detail'];
	$results = mysqli_query($mysqli, "SELECT id, name, style, platform, description FROM artist WHERE id=$id");
		  $row = mysqli_fetch_array($results) ?>
		<div class="detail">
			<h1><?php echo $row['name']; ?></h1>
			<h2><?php echo $row['style']; ?></h2>
			<h2><?php echo $row['platform']; ?></h2>
			<h2><?php echo $row['description']; ?></h2>
			<td>
			<a href="edit.php?edit=<?php echo $row['id']; ?>" class="edit_btn" >&#128396;</a>
			</td>
			<td>
				<a href="index.php?del=<?php echo $row['id']; ?>" class="del_btn" onclick="return confirm('Are you sure you want to Delete this?');">&#128465;</a>
			</td>
		</div>
	
<a href="#" onclick="history.back();"><button type="button" name="back" class="btn">Back</button></a>       
</body>
</html>