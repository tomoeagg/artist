<?php
require_once 'connect.php'
?>
<?php

	
	if (isset($_POST['edit'])) {
	$id = $_GET['edit']; 
	$name = $_POST['name'];
	$style = $_POST['style'];
	$platform = $_POST['platform'];
	$description = $_POST['description'];

	mysqli_query($mysqli, "UPDATE artist SET name='$name', style='$style', platform='$platform', description='$description' WHERE id=$id");
header('location: index.php');
}
?>
<html>
	<head>
		<title>MyArtist Edit</title>
		<h1 style="font-size: 500%;">&#120080;yartist</h1>
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="style.css">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body>
		<?php 
				$id = $_GET['edit']; 
				$results = mysqli_query($mysqli, "SELECT id, name, style, platform, description FROM artist WHERE id=$id");
					$row = mysqli_fetch_array($results);
					 $name = $row['name'];
					 $style = $row['style'];
					 $platform = $row['platform'];
					$description = $row['description'];

		?>	
		<form method="post" enctype="multipart/form-data">
		<h1 style= "size: 200%" align="center">Edit Artist's information</h1>
		<div class="input-group">
			<label>Name</label>
			<input required="" type="text" name="name" value="<?php echo $name; ?>">
		</div>
		<div class="input-group">
			<label>Style</label>
			<input type="text" name="style" value="<?php echo $style; ?>">
		</div>
		<div class="input-group">
			<label>Platform</label>
			<input type="text" name="platform" value="<?php echo $platform; ?>">
		</div>
		<div class="input-group">
			<label>Description</label>
			<input type="text" name="description" value="<?php echo $description; ?>">
		</div>
		<div class="input-group">
			<button class="btn" type="submit" name="edit" align="center">Confirm</button>
		</div>
		</form>
			<a href="#" onclick="history.back();"><button type="button" name="back" class="btn">Back</button></a>
	
	</body>
</html>