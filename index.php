<!DOCTYPE html>
<html>
    <head>
    <title>MyArtist by Lien Tran</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="style.css">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <h1 style="font-size: 500%">&#120080;yartist</h1>
    <h2 style="font-style: italic; font-size: 150%">Archive your favorite artists<br>Find your inspiration<br>Get creative</h2>
<?php
require_once 'connect.php'
?>
    </head>
    <body>	
<div class="row">
    <div class="col-sm-4">
    <form action="connect.php" method="POST" style="width: 90%; margin: 30px">
    	<h2 style="font-size: 200%">Add Artist</h2>
        <div class="input-group">
			<label>Name</label>
			<input type="text" name="name" placeholder="Enter artist's name or nickname" required="">
		</div>
		<div class="input-group">
			<label>Style</label>
			<input type="text" name="style" placeholder="Enter artist's style (e.g. Traditional, etc.)">
		</div>
		<div class="input-group">
			<label>Platform</label>
			<input type="text" name="platform" placeholder="Enter platforms on which the artist's active">
		</div>
		<div class="input-group">
			<label>Description</label>
			<input type="text" name="description" placeholder="More about this artist">
		</div>
        <div class="input-group">    
        <button  class="btn" type="submit" name="add" align="center" style="padding: 10px 30px" >Add</button>
    	</div>
    </form>
    </div>
    <div class="col-sm-8">
    	<form style="border: none;"action="search.php" method="GET">
    		<input type="text" name="search" placeholder="Find your inspiration" required="">
    		<button class="btn" name="submit-search" type="submit">Search</button>
    	</form>   
    	<div class="container">
    		<h2 style="size: 200%">Artist List</h2>
    		<?php 
    	
		$result = mysqli_query($mysqli,"SELECT id, name, style, platform, description FROM artist ");
		$queryResults = mysqli_num_rows($result);

		echo "<h4>"."Total of ".$queryResults." Artist(s)"."</h4>";
?>
    	
    <table class="table" style="background-color: rgba(20, 20, 20, 0.4);">
    	<thead>
    		<tr>
    			<th>Name</th>
    			<th>Style</th>
    			<th>Platform</th>
    			<th>Description</th>
    			<th colspan="3" style="text-align: center;">More</th>
    		</tr>
    	</thead>
<?php
	if (isset($_GET['del'])) 
	{
		$id = $_GET['del'];
		mysqli_query($mysqli, "DELETE FROM artist WHERE id=$id");
	}
?>
<?php 
$result = mysqli_query($mysqli, 'SELECT count(id) AS total FROM artist');
        $row = mysqli_fetch_array($result);
        $total_records = $row['total'];
        $current_page = isset($_GET['page']) ? $_GET['page'] : 1;
        $limit = 5;
        $total_page = ceil($total_records / $limit);
        if ($current_page > $total_page){
            $current_page = $total_page;
        }
        else if ($current_page < 1){
            $current_page = 1;
        }
        $start = ($current_page - 1) * $limit;
        $result = mysqli_query($mysqli, "SELECT id, name, style, platform, description FROM artist LIMIT $start, $limit");
        ?>
       
        <div>
            <?php 
            while ($row = mysqli_fetch_array($result)) { ?>
		<tr onmouseover="style='background-color: white; color:black'" onmouseleave="style='background-color: rgba(20, 20, 20, 0.2); color: white'">			
			<td><?php $string = $row['name'];
		if (strlen($string) > 20) {
			$trimstring = substr($string, 0, 20). '...';
			} else {
			$trimstring = $string;
			}
			echo $trimstring; ?></td>
			<td><?php $string = $row['style'];
		if (strlen($string) > 12) {
			$trimstring = substr($string, 0, 12). '...';
			} else {
			$trimstring = $string;
			}
			echo $trimstring; ?></td>
			<td><?php $string = $row['platform'];
		if (strlen($string) > 12) {
			$trimstring = substr($string, 0, 12). '...';
			} else {
			$trimstring = $string;
			}
			echo $trimstring; ?></td>
			<td><?php 
			 $string = $row['description'];
		if (strlen($string) > 12) {
			$trimstring = substr($string, 0, 12). '...';
			} else {
			$trimstring = $string;
			}
			echo $trimstring;
			?>
			</td>
			<td>
				<a href="detail.php?detail=<?php echo $row['id']; ?>" class="detail_btn" >&#128466;</a>
			</td>
			<td>
				<a href="edit.php?edit=<?php echo $row['id']; ?>" class="edit_btn" >&#128396;</a>
			</td>
			<td>
				<a href="index.php?del=<?php echo $row['id']; ?>" class="del_btn" onclick="return confirm('Are you sure you want to Delete this?');">&#128465;</a>
			</td>
		</tr>
	<?php } ?>
           </table>
        </div>
   <div class="btn-group" role="group">    
           <?php 
           if ($current_page > 1 & $total_page > 2){
                echo '<a href="index.php" class="btn">&#8810;</a>';
            }
            if ($current_page > 1 && $total_page > 1){
                echo '<a href="index.php?page='.($current_page-1).'" class="btn"><</a>';
            }
            for ($i = 1; $i <= $total_page; $i++){
                if ($i == $current_page){
                    echo '<span class="btn" style="background-color: white; color: black">'.$i.'</span>';
                }
                else{
                    echo '<a href="index.php?page='.$i.'"class="btn">'.$i.'</a>';
                }
            }
            if ($current_page < $total_page && $total_page > 1){
                echo '<a href="index.php?page='.($current_page+1).'" class="btn">></a>';
            }
            if ($current_page >= 1 & $current_page < $total_page & $total_page > 2){
                echo '<a href="index.php?page='.($total_page).'" class="btn">&#8811;</a>';
            }
           ?>   
</div>
</div>
</div>
        </div>
    </body>
</html>    
